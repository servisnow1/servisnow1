<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;
use App\Traits\Uuids;
class Admin extends Authenticable 
{
    use Uuids;
    protected $table = 'admins';
    protected $guarded = ['id'];
    protected $guard = 'admin';

    protected $fillable = ['username','name','password'];

    protected $hidden = ['password','remember_token'];
}
