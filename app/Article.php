<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Author;
use App\Tag;
class Article extends Model
{
    protected $fillable = ['title','content','picture'];

    public function authors(){
        return $this->BelongsTo(Author::class);
    }
    public function tags(){
        return $this->hasMany(Tag::class);
    }
}
