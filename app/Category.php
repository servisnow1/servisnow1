<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Merchant;
class Category extends Model
{
    
    protected $fillable = ['name','description'];

    

    public function merchants(){
        return $this->hasMany(Category::class);
    }
}
