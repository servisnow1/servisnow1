<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Validator;
use App\Category;
class AdminCategoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function showCategories(){
        return view('Admin.Categories.Panel');
      }

    public function indexCategories()
      {
          return \DataTables::of(Category::query())->make(true);
      }

 public function showFormCategory()
      {
         return view('Admin.Categories.Form');
      }

public function storeCategory(Request $request)
      {
        $rules = [
            'name' => 'required|unique:categories',
            'description' => 'required',
          ];
  
          $validator = Validator::make($request->all(), $rules);
          if ($validator->fails())
          return response()->json([
            'fail' => true,
            'errors' => $validator->errors()
          ]);
  
          $category = new Category();
          $category->name = $request->name;
          $category->description = $request->description;
          $category->save();
  
          return response()->json([
            'fail' => false,
            'redirect_url' => route('admin.categories')
          ]);
      
  
      }

      
}
