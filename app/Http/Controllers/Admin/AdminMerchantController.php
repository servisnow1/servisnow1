<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DataTables;
use Validator;
use App\Merchant;
class AdminMerchantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function showMerchants(){
        return view('Admin.Merchants.Panel');
      }
 public function indexMerchants()
      {
          return \DataTables::of(Merchant::query())->make(true);
      }
public function showForm()
      {
         return view('Admin.Merchants.Form');
      }
public function storeMerchant(Request $request)
      {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:merchants',
            'password' => 'required|min:6',
            'cpassword' => 'required|min:6|same:password',
            'city' => 'required|min:6',
            'address' => 'required',
            'phone' => 'required|min:6',
            'description' => 'required',

          ];
  
          $validator = Validator::make($request->all(), $rules);
          if ($validator->fails())
          return response()->json([
            'fail' => true,
            'errors' => $validator->errors()
          ]);
  
          $merchant = new Merchant();
          $merchant->name = $request->name;
          $merchant->email = $request->email;
          $merchant->password = bcrypt($request->password);
          $merchant->city = $request->city;
          $merchant->address = $request->address;
          $merchant->phone = $request->phone;
          $merchant->description = $request->description;
          $merchant->save();
  
          return response()->json([
            'fail' => false,
            'redirect_url' => route('create.merchant')
          ]);
      
  
        }
}
