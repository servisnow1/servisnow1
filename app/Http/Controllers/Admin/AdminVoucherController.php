<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Voucher;
use App\image;
use App\Category;
use Carbon\Carbon;
use App\Merchant;
use Validator;
class AdminVoucherController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
        
    }

    public function indexVouchers()
      {
          $vouchers = Voucher::with('images')->get();
          //$decode = json_decode($vouchers, true);
        //dd($vouchers);
          
         return view('Admin.Vouchers.Index',['vouchers' => $vouchers
         ]);
      }

      

     
      public function showFormVouchers()
      {
          $category = Category::all();
          $merchant = Merchant::all();
          return view('Admin.Vouchers.Form')->with(['categories' => $category,
          'merchants' => $merchant
          ]);
      }

      

        public function storeVouchers(Request $request)
        {
            
            $this->validate($request, [
                'name' => 'required',
                'photos' => 'required',
                'photos.*' => 'image|mimes:jpeg,png,jpg|max:2048',
                'priceNormal' => 'required|numeric',
                'priceDiscount' => 'required|numeric',
                'amount' => 'required|integer',
                'description' => 'required',
                'details' => 'required'

        ]);
        
                    


        $detail = $request->details;
        $dom = new \domdocument();
        $dom->loadHtml($detail, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $detail = $dom->savehtml();
        $dateStart = Carbon::parse($request->startFrom)->format('Y-m-d H:i:s');

        $dateExpired = Carbon::parse($request->expiredTo)->format('Y-m-d H:i:s');

        $voucher = new Voucher();
        $voucher->name = $request->name;
        $voucher->description = $request->description;
        $voucher->start_from = $dateStart;
        $voucher->expired_on = $dateExpired;
        $voucher->value = $request->amount;
        $voucher->price_normal = $request->priceNormal;
        $voucher->price_discount = $request->priceDiscount;
        $voucher->merchant_id = $request->merchant;
        $voucher->categories_id = $request->category;
        $voucher->details = $detail;
        $voucher->save();




        
         if($request->hasfile('photos'))
         {
             //$names=[];
            foreach($request->file('photos') as $photo)
            {
                $image = new image;
                $filename = $photo->getClientOriginalName();
                $extension = $photo->getClientOriginalExtension();
                $name = str_random(5)."-".date('his')."-".str_random(3).".".$extension;
                $destinationPath = base_path('public/images' . '/'.$request->merchant);
                $photo->move($destinationPath, $name);
                //$photo->move(public_path(). '\images\\' .$request->merchant , $name);
               // array_push($names, $name); 
               $image->voucher_id = $voucher->id;
               $image->name = $name;
               $image->save();        
            }

           
            
            
            
            
            
         }
         return response()->json([
            'message' => 'upload file successfully'
        ]);
            
        

    }
        

       
}
