<?php

namespace App\Http\Controllers\Backer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Backer;
use Auth;
use Validator;
use Hash;
class LoginController extends Controller
{
    public function redirectTo()
    {

        if($user->role == 'Admin')
        {
            return $this->adminPanel();
        }

        elseif($user->role == 'Admin')
        {
            return $this->managerPanel();
        }

        elseif($user->role == 'Admin')
        {
            return $this->developerManager();
        }

        else
        {
            return 404;
        }
    }
    public function redirectToLogin()
    {
        return view('Backer.login');
    }
  public function postLogin(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'username' => 'required|min:5',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('backer/')
                        ->withErrors($validator)
                        ->withInput();
        }

        else{
           
            $userdata = array(
                'username'     => $request->input('username'),
                'password'     => $request->input('password')
            );

            $user = Backer::where('username', $request->input('username'))
            ->first();
        
            // attempt to do the login
            if ($user && Hash::check($request->input('password'), $user->password)) {
        
                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
        if($user->role == 'Admin')
        {
            return $this->adminPanel();
        }

        elseif($user->role == 'Admin')
        {
            return $this->managerPanel();
        }

        elseif($user->role == 'Admin')
        {
            return $this->developerManager();
        }

        
         } else {        
        
                // validation not successful, send back to form 
                return redirect()->to('backer/');
        
            }
        
        
        }
       
    }
    
    
    protected function adminPanel()
    {
        return view('Admin.Panel');
    }

    
    protected function managerPanel()
    {
        return view('Manager.Homepage');
    }

    
    protected function developerPanel()
    {
        return view('Developer.Homepage');
    }
}
