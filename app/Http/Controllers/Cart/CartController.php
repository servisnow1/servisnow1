<?php

namespace App\Http\Controllers\Cart;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Voucher;
use App\image;
use \Cart as Cart;
use App\Order;
use App\Ordered;
use App\Detail;
use App\Merchant;
use Validator;
use Auth;
use RtLopez\Decimal;
use App\UserVouchers;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartContent = Cart::getContent();
        if($cartContent->isEmpty()){
           return view('cart.index'); 
        }
        
        //dd($cartContent);
       //$image = $details[0]['images'][0]['name'];
        //$merchant = $details[0]['merchant_id'];
        
        return view('cart.index',[
            'details' => $cartContent,
            
        ]);
        
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $id = decrypt($request->id);
        $name = decrypt($request->name);
        $priceDiscount = decrypt($request->priceDiscount);
        $image = decrypt($request->image);
        $slug = decrypt($request->slug);
        //Cart::add(array('id' => $request->id, 'name' => $request->name, 'qty' => 1, 'price' => $request->price, 'image' => $request->image));
        Cart::add($id, $name, $priceDiscount, $request->Qty, $image, $slug);
        //$content = Cart::content();
        //dd($content);
        $jumlahCart = Cart::getContent();
        $count = $jumlahCart->count();
        return response()->json([
            'success' => True,
            'jumlah' => $count,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,5'
        ]);

         if ($validator->fails()) {
            //session()->flash('error_message', 'Quantity must be between 1 and 5.');
            return response()->json([
                'success' => false,

                
                ]);
         }

        Cart::update($id, array(
            'quantity' => array(
                'relative' => false,
                'value' => $request->quantity
            ),));
        //session()->flash('success_message', 'Quantity was updated successfully!');

        return response()->json(['success' => true,
        'qty' => 'quantity'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return redirect()->to('/');
    }

    public function emptyCart()
    {
        Cart::clear();
        return redirect()->to('/');
    }

    public function checkOut(Request $request)
    {
       
       
       
        if(!Auth::user())
        {
            return redirect()->route('login');
        }
        
        
        
        
        
        //$order_details = [];
        $contents = Cart::getContent();
        foreach($contents as $content)
        {
            $voucher = Voucher::where('id', $content->id)->first();
            $MerchantID = Merchant::where('id', $voucher->merchant_id)->first();
            
            $order = new Order();
            $order->grand_total = Cart::getTotal();
            $order->customer_id = Auth::user()->id; 
            $order->save();

            
            $orderDetails = new Ordered();
            $orderDetails->order_id = $order->id;
            $orderDetails->voucher_id = $content->id;
            $orderDetails->qty = $content->quantity;
            $orderDetails->price = $content->price;
            $orderDetails->total_price = $orderDetails->quantity * $orderDetails->price;
            $orderDetails->save();

            $userVouchers = new UserVouchers();
            $userVouchers->user_id = Auth::user()->id;
            $userVouchers->voucher_id = $content->id;
            $userVouchers->qty = $content->quantity;
            $userVouchers->unique_code = str_random(6);
            $userVouchers->save();
        }
        Cart::clear();
        return redirect()->to('/home');
       
    }
}
