<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Voucher;
use App\image;
use \Cart as Cart;
use Validator;
use Carbon\Carbon;
class IndexController extends Controller
{
    public function index()
    {
       $cartContent = Cart::getContent();
        $count = $cartContent->count();
        $now = Carbon::now();
        $vouchers = Voucher::with('images')->where('expired_on','>', $now)->inRandomOrder()->limit(9)->get();
        
        
        
        
        
        return view('homepage',['vouchers' => $vouchers, 'isiCart' => $cartContent,
        ]);
        
    }
    public function showDetails($slug)
    {
        //$details = Voucher::where('id', $request->idV)->get();
        $details = Voucher::with('images')->where('slug' , $slug)->firstOrFail();

        //dd($details);
      

        return view('details',[
            'vouchers' => $details,
            'images' => $details->images,
            
        ]);
  
    }

    

    public function shopAll()
    {
        $now = Carbon::now();
        $vouchers = Voucher::with('images')->where('expired_on','>', $now)->paginate(12);
        return view('shop',[
            'vouchers' => $vouchers
        ]);
    }
}
