<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserVouchers;
use App\User;
use App\image;
use App\Voucher;
use App\Merchant;
use Auth;
use Carbon\Carbon;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Customer.home');
    }

    public function myVouchers($id)
    {
        $now = Carbon::now();
        $customerID = User::find($id)->vouchers()->get();
        //$show = $customerID->where('expired_on' > $now)->get();
      
      
       
      return view('Customer.voucher',[
       'vouchers' => $customerID,
       ]);
      
      
    
    }
    public function myVoucherDetails($uniqueCode)
    {
        $uniqueCode = decrypt($uniqueCode);
        
            $voucherDetails = UserVouchers::where('unique_code', $uniqueCode)->get();
      foreach($voucherDetails as $details)
      {
          $vouchersID = $details->voucher_id;
          $totalQty = $details->qty;
      }
      $voucher = Voucher::with('images')->find($vouchersID);
      $price = $voucher['price_discount'];
      $total = $totalQty * $price;
      $merchantID = $voucher['merchant_id'];
      $merchantData = Merchant::find($merchantID);
    return view('Customer.voucher.detail',[
        'details' => $voucherDetails,
        'isi' => $voucher,
        'grandTotal' => $total,
        'merchants' => $merchantData,
       ]);
     
        
      
    
    }
}
