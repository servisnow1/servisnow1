<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;
class MerchantLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:merchant', ['except' => ['logout']]);
    }
    
    public function showLoginForm()
    {
      return view('Merchant.login');
    }
   
    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      
      // Attempt to log the user in
      if (Auth::guard('merchant')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('merchant.dashboard'));
      } 
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    
    public function logout()
    {
        Auth::guard('merchant')->logout();
        return redirect()->to('/merchant/login');
    }
}
