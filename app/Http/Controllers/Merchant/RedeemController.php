<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Voucher;
use App\User;
use App\Redeem;
use Auth;
use App\RedeemDetail;
use App\UserVouchers;
use Validator;
use Carbon\Carbon;
use App\Merchant;
class RedeemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:merchant');
    }
    public function redeemView()
    {
        return view('Merchant.Redeem.Form');
    }

    public function redeemVoucherForm(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'kode_unik' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('/redeem')
                        ->withErrors($validator)
                        ->withInput();
        }
        $check = UserVouchers::where('unique_code', $request->kode_unik)->first();
       // dd($check);
        //$unique_code = decrypt($kode_unik);
        //$check = UserVouchers::where('unique_code', $unique_code)->first();
        //$check = UserVouchers::where('unique_code', $unique_code)->first();
        //$check = Redeem::where('unique_code', $unique_code)->first();
        $now = Carbon::now()->format('Y-m-d H:i:s');
        
            $voucherID = $check->voucher_id;
            $voucher = Voucher::where('id',$voucherID)->first();
            $expired = $voucher->expired_on;
            $price = $voucher->price_discount;
            $qty = $check->qty;

            
        if($check->exists() && $expired >= $now)
        {
            
            
            $redeem = new Redeem();
            $redeem->unique_code = $check->unique_code;
            $redeem->merchant_id = Auth::user()->id;
            $redeem->amount = $qty * $price;
            $redeem->redeem_at = $now;
            $redeem->user_id = $check->user_id;
            $redeem->save();

            $voucher = Voucher::find($voucherID);
            $voucherAmount = $voucher->value;
            $updateAmountVoucher = $voucherAmount - $qty;
            $voucher->value = $updateAmountVoucher;
            $voucher->save(); 

            return response()->json([
                'message' => 'Redeem voucher successfully'
            ]);

        }

        return abort(404);
        
    }

    public function redeemVoucher($kode_unik)
    {
        $unique_code = decrypt($kode_unik);
        $check = UserVouchers::where('unique_code', $unique_code)->first();
        $check2 = Redeem::where('unique_code', $unique_code)->first();
        $now = Carbon::now();
        
        $voucherID = $check->voucher_id;
            $voucher = Voucher::where('id',$voucherID)->first();
            $price = $voucher->price_discount;
            $qty = $check->qty;

        if($check->exists() && $voucher->expired_on < $now)
        {
            
            
            $redeem = new Redeem();
            $redeem->unique_code = $check->unique_code;
            $redeem->merchant_id = Auth::user()->id;
            $redeem->amount = $qty * $price;
            $redeem->redeem_at = $now;
            $redeem->user_id = $check->user_id;
            $redeem->save();

            $voucher = Voucher::find($voucherID);
            $voucherAmount = $voucher->value;
            $updateAmountVoucher = $voucherAmount - $qty;
            $voucher->value = $updateAmountVoucher;
            $voucher->save(); 

            return view('redeem.success');

        }

        return abort(404);
        /*
        foreach($check as $item)
        {
           $voucher_id = $check->voucher_id;
        }
        $voucherData = Voucher::where('id',$voucher_id)->first();
        
        foreach($voucherData as $data)
        {
            $dateExpired = $data->expired_on;
        }
        $checkDifferent = $dateExpired->diffInDays($now);
        dd($checkDifferent);
        /*
        if($check === null)
        {
            abort(404);
        }
        elseif($check2->exist())
        {
            abort(404);
        }
        elseif($now > $dateExpired)
        {
            abort(404);
        }
        else{
            $redeem = new Redeem();
            $redeem->unique_code = $unique_code;
            $redeem->vouhcer_code = $check->voucher_id;
            $redeem->merchant_id = Auth::user()->id;
            $redeem->redeem_at = $now;
            $redeem->save();
        }

        */
    }
}
