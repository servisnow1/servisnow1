<?php

namespace App\Http\Controllers\Merchant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Merchant;
use Validator;
use Carbon\Carbon;
use App\ResetPasswordMerchant;
use App\Http\Requests\SendEmailMerchants;
use App\Jobs\SendForgetPasswordMerchant;
class ResetPasswordController extends Controller
{
    public function resetView()
    {
        

        return view('Admin.Merchants.reset');
    }

    public function saveData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:merchants',
        ]);
        
        if($validator->fails())
        {
            return redirect()->back();
        }

        $numberRandom = str_random(6); 
        $token = bcrypt($numberRandom);
        $email = $request->email;
        $now = Carbon::now();
        $resetMerchantPassword = new ResetPasswordMerchant();
        $resetMerchantPassword->email = $email;
        $resetMerchantPassword->token = $token;
        $resetMerchantPassword->created_at = $now->format('Y-m-d H:i:s');
        $resetMerchantPassword->save();
        dispatch(new SendForgetPasswordMerchant($resetMerchantPassword));
        return response()->json([
            'message' => 'A message has been sent to your email. Please check your inbox.'
        ], 200);
        


    }

   
}
