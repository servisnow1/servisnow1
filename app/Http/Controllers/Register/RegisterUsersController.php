<?php

namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class RegisterUsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function createUsers(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'sex' => 'required',
            'phone' => 'required|string',
            'birthday' => 'required|date'

        ]);
        $birthday = Carbon::parse($request->birthday)->format('d-m-Y');
    $users = new User;
    $users->name = $request->name;
    $users->email = $request->email;
    $users->phone_number = $request->phone;
    $users->password = bcrypt($request->password);
    $users->sex = $request->SexRadio;
    $users->birthday = $birthday;
    $users->save();

    return redirect()->to('/');
    }
}
