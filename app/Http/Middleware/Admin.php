<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && Auth::user()->role== 'Admin')
        {
            return $next($request);
        }

        elseif(Auth::check() && Auth::user()->role == 'Manager')
        {
            return redirect()->to('backer/manager');
        }

        elseif(Auth::check() && Auth::user()->role == 'Developer')
        {
            return redirect()->to('backer/manager');
        }
        
    }
}
