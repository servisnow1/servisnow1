<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
class AuthResource
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->route('/myvouchers/{id}')) {
            $customer = User::find($request->route('id'));
            if ($customer && $customer->id != auth()->user()->id) {
                return redirect()->back();
            }
        }
        return $next($request);
    }
}
