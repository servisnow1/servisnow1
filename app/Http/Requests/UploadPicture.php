<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadPicture extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'photo' => 'required|array',
            'photo.*' => 'required|image|mimes:jpeg,bmp,png|max:2000',
            'start-from' => 'required|date',
            'expired_until' => 'required|date|after:start-from',
            'price_normal' => 'required|integer',
            'price_discount' => 'required|integer',
            'description' => 'required'
        ];
    }
}
