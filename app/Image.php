<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Voucher;
class image extends Model
{
    protected $table = 'images';

    protected $fillable = ['name', 'voucher_id'];

    public function vouchers()
    {
        return $this->belongsTo(Voucher::class,'voucher_id');
    }
    
    
    
    public function users()
    {
        return $this->hasManyThrough(
            'App\User',
            'App\UserVouchers',
            'voucher_id',
            'user_id',
            'id',
            'voucher_id'
        );
    }

   
    
}
