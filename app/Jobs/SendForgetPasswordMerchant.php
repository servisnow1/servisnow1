<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Mail\SendMerchantResetPassword;
class SendForgetPasswordMerchant implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $resetMerchantPassword;
    public function __construct($resetMerchantPassword)
    {
        $this->resetMerchantPassword = $resetMerchantPassword;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new SendMerchantResetPassword($this->resetMerchantPassword);
        Mail::to($this->resetMerchantPassword->email)->send($email);
    }
}
