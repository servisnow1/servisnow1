<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Merchant;
class SendMerchantResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $resetMerchantPassword;

    public function __construct($resetMerchantPassword)
    {
        $this->resetMerchantPassword = $resetMerchantPassword;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.forget')->with([
            'email' => $this->resetMerchantPassword->email,
            'token' => $this->resetMerchantPassword->token,
        ]);
    }
}
