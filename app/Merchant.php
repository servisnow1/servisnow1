<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticable;
use App\Category;
class Merchant extends Authenticable 
{
    
    protected $fillable = ['name','email','password','city','address','phone'];
    protected $hidden = ['password'];

    public function categories()
    {
        return $this->belongsTo(Category::class);
    }
   
}
