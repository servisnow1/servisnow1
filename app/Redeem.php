<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Merchant;
use App\UserVouchers;
use App\RedeemDetail;
use App\Voucher;
class Redeem extends Model
{
    public $timestamps = false;
    protected $table = 'redeems';
    protected $fillable = ['unique_code','merchant_id','voucher_id','amount','redeem_at'];
    public function merchants()
    {
        return $this->belongsTo(Merchant::class);
    }
    public function vouchers()
    {
        return $this->belongsTo(Voucher::class);
    }
    public function uservouchers()
    {
        return $this->belongsToOne(UserVouchers::class);
    }

    public function redeemDetails()
    {
        return $this->hasOne(RedeemDetail::class);
    }
    
}
