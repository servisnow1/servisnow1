<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Redeem;
use App\Voucher;
class RedeemDetail extends Model
{
    protected $table = 'redeem_details';

    protected $fillable = [];



    public function Redeems()
    {
        return $this->belongsToOne(Redeem::class);
    }

    public function Vouchers()
    {
        return $this->hasOne(Voucher::class);
    }
}
