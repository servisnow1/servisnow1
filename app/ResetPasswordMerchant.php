<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResetPasswordMerchant extends Model
{
    
    protected $fillable = ['email','token','created_at'];
    public $timestamps = false;


    protected $table = 'reset_password_merchants';
}
