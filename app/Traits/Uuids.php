<?php
namespace App\Traits;

trait Uuids 
{
    /**
     * 
     */
    protected static function bootUuids()
    {
       
        static::creating(function ($model) {
            $model->id = 
            str_replace('-', '', \Uuid::generate(4)->string);
        });
    }
}