<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Voucher;
use App\Order;
class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password','phone_number', 'sex', 'birthday'
    ];
 

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class,'user_vouchers')->as('myVouchers')->withPivot('qty','unique_code');
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    
}
