<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserVouchers extends Pivot
{
    protected $table = 'user_vouchers';
    public $timestamps = false;
    protected $fillable = ['user_id','voucher_id','qty','unique_code'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function voucher()
    {
        return $this->belongsTo('App\Voucher','id');
    }

    public function images()
    {
        return $this->hasManyThrough('App\image', 'App\Voucher');
    }
}
