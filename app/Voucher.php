<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ordered;
use App\Category;
use Carbon\Carbon;
use App\image;
use App\Merchant;
use App\RedeemDetail;
use Cviebrock\EloquentSluggable\Sluggable;
class Voucher extends Model
{
    use Sluggable;
    protected $fillable = ['name','start_from','expired_on','price_normal','price_discount','status','value','slug'];
    
    protected $table = 'vouchers';

    public function details()
    {
        return $this->belongsToOne(ordered::class);
    }

    public function redeemDetails()
    {
        return $this->belongsToOne(RedeemDetail::class);
    }
    
    public function categories()
    {
        return $this->belongsTo(Category::class);
    }

    public function merchants()
    {
        return $this->belongsTo(Merchant::class);
    }

    
    public function images() 
    {
        return $this->hasMany(image::class,'voucher_id');
    }


    

   
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function users()
    {
        return $this->belongsToMany('App\User','user_vouchers')->as('myVouchers')->withPivot('qty','unique_code');
    }

}
