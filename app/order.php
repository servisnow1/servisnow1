<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Ordered;
use App\User;
class order extends Model
{
    protected $table = "orders";

    protected $fillable = ['grand_total','status','customer_id','merchant_id'];

    public function Ordered()
    {
        return $this->hasMany(Ordered::class);
    }

    public function Users()
    {
        return $this->belongsTo(User::class);
    }

    protected $casts = [
        'grand_total' => 'decimal:2',
    ];

    



   
}
