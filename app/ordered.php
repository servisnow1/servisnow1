<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Voucher;
class ordered extends Model
{
    protected $table = "order_details";
    protected $fillable = ['order_id','voucher_id','qty','price','serial_numberID'];

    public function Orders()
    {
        return $this->belongsTo(order::class);
    }

    public function Vouchers()
    {
        return $this->hasOne(Voucher::class);
    }

   
}
