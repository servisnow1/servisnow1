<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedeemDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redeem_details', function (Blueprint $table) {
            $table->integer('redeem_id')->unsigned();
            $table->foreign('redeem_id')->on('redeems')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('voucher_id')->unsigned();
            $table->foreign('voucher_id')->on('vouchers')->references('id')->onDelete('cascade')->onUpdate('cascade');
            $table->decimal('price', 14, 2);
            $table->integer('qty');
            $table->decimal('sub_total', 14, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redeem_details');
    }
}
