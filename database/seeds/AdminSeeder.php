<?php

use Illuminate\Database\Seeder;
use App\Admin;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new Admin();
        $admin->name = 'Admin Ganteng';
        $admin->username = 'Admin';
        $admin->password = bcrypt('IniAdalahN4r4Hub');
        $admin->save();
    }
}
