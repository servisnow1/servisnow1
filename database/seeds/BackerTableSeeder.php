<?php

use Illuminate\Database\Seeder;
use App\Backer;
class BackerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    $admin = new Backer();
    $admin->name = 'Admin';
    $admin->username = 'Admin';
    $admin->password = bcrypt('IniAdalahN4r4Hub');
    $admin->role = 'Admin';
    $admin->save();
    
    $manager = new Backer();
    $manager->name = 'Manager';
    $manager->username = 'Manager';
    $manager->password = bcrypt('IniAdalahUs3rM4n4g3r');
    $manager->role = 'Manager';
    $manager->save();
    
    $super = new Backer();
    $super->name = 'Super User';
    $super->username = 'Developer';
    $super->password = bcrypt('IniAdalahD3v3l0p3r');
    $super->role = 'Developer';
    $super->save();
    
    }
}
