<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
  {
    $role_admin = new Role();
    $role_admin->name = 'Admin';
    $role_admin->description = 'A Employee admin';
    $role_admin->save();
    $role_manager = new Role();
    $role_manager->name = 'Manager';
    $role_manager->description = 'A Manager User';
    $role_manager->save();
    $role_super = new Role();
    $role_super->name = 'Super User';
    $role_super->description = 'A Super User';
    $role_super->save();
  }
}
