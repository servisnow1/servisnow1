$(document).ready(function(){

    
function formResetMerchantPasswordSubmit()
    {
   
        $(document).on('submit', 'form#resetPasswordMerchant', function (event) {
        event.preventDefault();
        var form = $(this);
        var data = new FormData($(this)[0]);
        var url = form.attr("action");
        $.ajax({
            type: form.attr('method'),
            url: url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                
                    $('#resetPasswordMerchant').find('input[type=email]').val('');
                    $.notify(data.message, "success");
                   
                
            },
            error: function (data) {
                
                    var errors = data.responseJSON; // An array with all errors.
                    console.log(errors);
            }
        });
        return false;
    });
}


});
