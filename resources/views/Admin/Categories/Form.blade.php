@extends('layouts.app')
    @section('title','Admin - Categories')
</head>
@section('content')
@include('navbar')
<div class="row">
<div class="col-sm-12 mt-3 mb-2 ml-2"><a href="{{ route('admin.categories')}}" class="btn btn-success"><i class="fas fa-angle-left"></i>&nbsp;Back</a></div>
</div>

    <div class="row justify-content-center mt-3">
        <div class="col-sm-12">
        <div class="container">
            <div class="card mb-3">
                <div class="card-header text-center" style="background-color: #fff;"> Add Categories </div>

                <div class="card-body">
                    <form method="POST" id="frm" action="{{ route('store.categories') }}" aria-label="{{ __('Add Category') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="category" class="col-sm-3 col-form-label text-md-right">{{ __('Nama Kategori') }}</label>

                            <div class="col-md-9">
                                <input id="name" placeholder="Masukkan nama kategori" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-lg" name="name" required autofocus>

                                @if ($errors->has('name'))
                                    <span id="error-name" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="description" class="col-md-3 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-9">
                                <input id="description" type="text" class="form-control-lg form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Masukkan description category" required>

                                @if ($errors->has('category'))
                                    <span id="error-email" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    <div>
                    
                    
                </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                <i class="far fa-save"></i>
                                    {{ __('Daftarkan') }}
                                </button>
                                <div class="message"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
@section('scripts')
<script>
$(document).ready(function(){
    $(document).on('submit', 'form#frm', function (event) {
    event.preventDefault();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: form.attr('method'),
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $('.is-invalid').removeClass('is-invalid');
            if (data.fail) {
                for (control in data.errors) {
                    $('#' + control).addClass('is-invalid');
                    $('#error-' + control).html(data.errors[control]);
                }
            } else {
                $('#frm').find('input:text').val('');
                $.notify("Input data has been added successfully", "success");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
    return false;
});
});
    
</script>
@endsection