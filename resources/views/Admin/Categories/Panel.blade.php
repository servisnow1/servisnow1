@extends('layouts.app')
@section('title','Admin - Categories')
@section('content')
@include('navbar')
<div class="container mt-5">
               <h2 class="text-center">Data Categories</h2> <a href="{{ route('create.categories') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Add Category</a>
            <br><br>
            <table class="table table-bordered" id="table">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Description</th>
                  </tr>
               </thead>
            </table>
         </div>
         @section('scripts')
       <script>
         $(function() {
               $('#table').DataTable({
               processing: true,
               serverSide: true,
               ajax: '{{ route('index.categories') }}',
               columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'description', name: 'description' },
               ]
            });
    });
</script>
 @endsection
