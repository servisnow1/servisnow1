<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description">
    <title>Create Merchant - Narahub Admin</title>
    @include('layouts.style')
</head>
<body>
<main>
@include('navbar')
<div class="row">
<div class="col-sm-12 mt-3 mb-2 ml-2"><a href="{{ route('admin.merchants')}}" class="btn btn-success"><i class="fas fa-angle-left"></i>&nbsp;Back</a></div>
</div>

    <div class="row justify-content-center mt-3">
        <div class="col-sm-12">
        <div class="container">
            <div class="card mb-3">
                <div class="card-header text-center" style="background-color: #fff;"> Add Merchant Account </div>

                <div class="card-body">
                    <form method="POST" id="frm" action="{{ route('store.merchant') }}" aria-label="{{ __('Add Merchant Account') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label text-md-right">{{ __('Nama Toko') }}</label>

                            <div class="col-md-9">
                                <input id="name" placeholder="Masukkan nama toko" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-lg" name="name" required autofocus>

                                @if ($errors->has('name'))
                                    <span id="error-name" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-9">
                                <input id="email" type="email" class="form-control-lg form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Masukkan alamat e-mail toko" required>

                                @if ($errors->has('email'))
                                    <span id="error-email" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-3 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-9">
                                <input id="password" type="password" class="form-control-lg form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span id="error-password" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="confirm-password" class="col-md-3 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-9">
                                <input id="cpassword" type="password" class="form-control-lg form-control{{ $errors->has('cpassword') ? ' is-invalid' : '' }}" name="cpassword" required>

                                @if ($errors->has('cpassword'))
                                    <span id="error-cpassword" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('cpassword') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="City" class="col-md-3 col-form-label text-md-right">{{ __('Kota') }}</label>

                            <div class="col-md-9">
                                <input id="city" type="text" class="form-control-lg form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" placeholder="Masukkan nama kota. Contoh : Surabaya" required>

                                @if ($errors->has('city'))
                                    <span id="error-city" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Address" class="col-md-3 col-form-label text-md-right">{{ __('Alamat Toko') }}</label>

                            <div class="col-md-9">
                                <input id="Address" type="text" class="form-control-lg form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="Masukkan alamat toko. Contoh : Jl Panglima Sudirman No 27, Kecamatan Kebomas, Kelurahan Sidomoro, Kabupaten Gresik" required>

                                @if ($errors->has('address'))
                                    <span id="error-address" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Phone" class="col-md-3 col-form-label text-md-right">{{ __('Nomor Telepon') }}</label>

                            <div class="col-md-9">
                                <input id="Phone" type="text" class="form-control-lg form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="Masukkan nomor telepon. Contoh : 031-12345678 atau 08123456789" required>

                                @if ($errors->has('phone'))
                                    <span id="error-phone" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Description" class="col-md-3 col-form-label text-md-right">{{ __('Informasi toko') }}</label>

                            <div class="col-md-9">
                                <textarea id="description" rows=6 class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Masukkan informasi mengenai merchant" required>
                                </textarea>
                                @if ($errors->has('description'))
                                    <span id="error-description" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    <div>
                    
                    
                </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                <i class="far fa-save"></i>
                                    {{ __('Daftarkan') }}
                                </button>
                                <div class="message"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>

</main>
    @include('layouts._include.plugin')
<script>
$(document).ready(function(){
    $(document).on('submit', 'form#frm', function (event) {
    event.preventDefault();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: form.attr('method'),
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            $('.is-invalid').removeClass('is-invalid');
            if (data.fail) {
                for (control in data.errors) {
                    $('#' + control).addClass('is-invalid');
                    $('#error-' + control).html(data.errors[control]);
                }
            } else {
                $('#frm').find('input:text, input:password, input[type=email], select, textarea').val('');
                $.notify("Input data has been saved successfully", "success");
               
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
    return false;
});
});
    
</script>
</body>
</html>