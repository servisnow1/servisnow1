<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Dashboard</title>

    @include('layouts.style')
    
  </head>

  <body id="page-top">
@include('navbar')
<body>
<div class="row">
<div class="col-sm-12 mr-auto">

</div>
</div>
         <div class="container mt-5">
               <h2 class="text-center">Data Merchants</h2> <a href="{{ route('create.merchant') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Add Merchant</a>
            <table class="table table-bordered" id="table">
               <thead>
                  <tr>
                     <th>Id</th>
                     <th>Name</th>
                     <th>Email</th>
                     <th>City</th>
                     <th>Address</th>
                     <th>Phone</th>
                  </tr>
               </thead>
            </table>
         </div>
         @include('layouts._include.plugin')
       <script>
         $(function() {
               $('#table').DataTable({
               processing: true,
               serverSide: true,
               ajax: '{{ route('merchants.index') }}',
               columns: [
                        { data: 'id', name: 'id' },
                        { data: 'name', name: 'name' },
                        { data: 'email', name: 'email' },
                        { data: 'city', name: 'city' },
                        { data: 'address', name: 'address' },
                        { data: 'phone', name: 'phone' },
                     ]
            });
         });
         </script>
  </body>

</html>
