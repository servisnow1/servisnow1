<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Dashboard</title>

    @include('layouts.style')
  
  </head>

  <body id="page-top">
@include('navbar')
   
<section id="welcome-page">
  <div class="container">
<div class="card mt-5">
  <div class="card-body">
    Welcome home, {{ Auth::user()->name }}
  </div>
</div>
</div>
</section>
@include('layouts._include.plugin')
  </body>

</html>
