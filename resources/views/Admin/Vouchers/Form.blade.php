@extends('layouts.app')
@section('title','Vouchers input admin')
@section('content')
<main>
@include('navbar')
<div class="row">
<div class="col-sm-12 mt-3 mb-2 ml-2"><a href="{{ route('admin.vouchers')}}" class="btn btn-success"><i class="fas fa-angle-left"></i>&nbsp;Back</a></div>
</div>

    <div class="row justify-content-center mt-3">
        <div class="col-sm-12">
        <div class="container">
           
                <h2 class="text-center">Input voucher</h2><br><br>
                    <form method="POST" id="frm" enctype="multipart/form-data" action="{{ route('store.vouchers') }}" aria-label="{{ __('Create vouchers') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label text-md-right">{{ __('Nama Voucher') }}</label>

                            <div class="col-md-9">
                                <input id="name" placeholder="Masukkan nama voucher. ex : Asus ROG X-415 Nvidia Geforce Surabaya" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-lg" name="name" required autofocus>

                                @if ($errors->has('name'))
                                    <span id="error-name" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    <div class="form-group row">
                        <label for="start-from" class="col-md-3 col-form-label text-md-right">{{ __('Start from') }}</label>
                    <div class="col-md-9">
                            
                        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                             <input type="text" class="form-control datetimepicker-input" name="startFrom" data-target="#datetimepicker1"/>
                            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                            
                        </div>
                    </div>
                    <div class="form-group row">
                            <label for="expired to" class="col-md-3 col-form-label text-md-right">{{ __('Expired Until') }}</label>
                        <div class="col-md-9">
                        <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                             <input type="text" class="form-control datetimepicker-input" name="expiredTo" data-target="#datetimepicker"/>
                            <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                        </div>
                    </div>
                        <div class="form-group row">
                            <label for="price_normal" class="col-md-3 col-form-label text-md-right">{{ __('Price normal') }}</label>

                            <div class="col-md-9">
                                <input id="priceNormal" type="text" placeholder="Masukkan nominal harga normal" class="form-control-lg form-control{{ $errors->has('priceNormal') ? ' is-invalid' : '' }}" name="priceNormal" required>

                                @if ($errors->has('priceNormal'))
                                    <span id="error-price_normal" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('priceNormal') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="priceDiscount" class="col-md-3 col-form-label text-md-right">{{ __('Price Discount') }}</label>

                            <div class="col-md-9">
                                <input id="price_discount" type="text" class="form-control-lg form-control{{ $errors->has('price_discount') ? ' is-invalid' : '' }}" name="priceDiscount" placeholer="Harga nominal setelah diskon" required>

                                @if ($errors->has('priceDiscount'))
                                    <span id="error-cpassword" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('priceDiscount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Amount" class="col-md-3 col-form-label text-md-right">{{ __('Amount') }}</label>

                            <div class="col-md-9">
                                <input id="amount" type="text" class="form-control-lg form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" placeholder="Masukkan jumlah voucher yang anda inginkan. Jika tidak terbatas masukkan 99. " required>

                                @if ($errors->has('amount'))
                                    <span id="error-amount" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Category" class="col-md-3 col-form-label text-md-right">{{ __('Category') }}</label>

                            <div class="col-md-9">
                                
                                <select id="Category" type="text" class="form-control-lg form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" name="category"  required>
                                @foreach($categories as $category)    
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                                
@if ($errors->has('category'))
                                    <span id="error-address" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="merchant" class="col-md-3 col-form-label text-md-right">{{ __('Merchant') }}</label>

                            <div class="col-md-9">
                                
                                <select id="merchants" type="text" class="form-control-lg form-control{{ $errors->has('merchant') ? ' is-invalid' : '' }}" name="merchant"  required>
                                @foreach($merchants as $merchant)    
                                <option value="{{ $merchant->id }}">{{ $merchant->name }}</option>
                                @endforeach
                            </select>
                                
                                @if ($errors->has('merchant'))
                                    <span id="error-address" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('merchant') }}</strong>
                                    </span>
                                @endif
                                <input type="hidden" name="merchantName" value="{{ $merchant->name }}" />
                            </div>
                        </div>
                        

                        <div class="form-group row">
                            <label for="Images" class="col-md-3 col-form-label text-md-right">{{ __('Upload Gambar') }}</label>

                            <div class="col-md-9 custom-file">
                            <input type="file" class="custom-file-input" name="photos[]" multiple />
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            @if (count($errors) > 0)
                                    <span id="error-photos" class="invalid-feedback" role="alert">
                                    @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
                                    </span>
                                @endif
                            </div>
                        </div>

                         
                        <div class="form-group row">
                            <label for="Description" class="col-md-3 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-9">
                                <textarea id="description" rows=6 class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Masukkan informasi mengenai voucher anda" required>
                                </textarea>
                                @if ($errors->has('description'))
                                    <span id="error-description" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Details" class="col-md-3 col-form-label text-md-right">{{ __('Details') }}</label>

                            <div class="col-md-9">
                                <textarea id="Detail" rows=6 class="form-control{{ $errors->has('details') ? ' is-invalid' : '' }} detail" name="details" placeholder="Masukkan informasi lengkap mengenai voucher anda. contoh: informasi tentang produk, syarat dan ketentuan, dan lain-lain" required>
                                </textarea>
                                @if ($errors->has('details'))
                                    <span id="error-details" class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('details') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    <div>
                    
                    
                </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                <i class="far fa-save"></i>
                                    {{ __('Daftarkan') }}
                                </button>
                                <div class="message"></div>
                            </div>
                        </div>
                    </form>
                </div>
            


</div>
</main>
@section('scripts')
<script type="text/javascript">

$(function () {
                $("#datepicker").datetimepicker();
  
                $("#datepicker1").datetimepicker();
                
                $('.detail').summernote();

$(document).on('submit', 'form#frm', function (event) {
    event.preventDefault();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: form.attr('method'),
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
             
                $('#frm').find('input:text, select, textarea').val('');
                $.notify("Input data has been saved successfully", "success");
               
            
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
    return false;
});

  
});

</script>
@endsection