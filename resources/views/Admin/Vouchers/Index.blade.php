@extends('layouts.app')
@section('title','Vouchers - Index')
@section('content')
@include('navbar')
<div class="row">
<div class="col-md-12"><h2 class="text-center mt-4">List Vouchers</h2> <a href="{{ route('create.vouchers') }}" class="btn btn-primary mb-2"><i class="fas fa-plus"></i> Create Voucher</a></div>
</div>
@foreach($vouchers->chunk(3) as $voucher)
    <div class="row mt-2 mb-2">
    @foreach($voucher as $items)
       <div class="col-md-4">
        
    
          
        <a href="{{ url('/vouchers/'.$items->slug)}}" class="card myBox">
             <img class="card-img"  src="{{ asset('images/' .$items->merchant_id. '/' .$items->images[0]->name ) }}" alt="Merchant1">
          <div class="card-body">
            <span class="card-title">{{ $items->name }} </span>
                <p>{{ $items->description}}</p>
          </div>
          <div class="card-footer">
            <p class="text-right mb-0"><del> Rp. {{ number_format($items->price_normal, 2, ',', '.') }}<del></p>
            <h3 class="text-right text-primary">Rp. {{ number_format($items->price_discount, 2, ',', '.') }}</h3>
          </div>
        </a>
      </div>
    @endforeach

      </div>
    </div>
   @endforeach    
</div>
 
@section('scripts')
@endsection