@extends('layouts.app')
@section('title','Narahub - Customer Page')
@section('content')
@include('layouts._include.header')
<div class="container">
<div class="row mt-4">
<div class="col-md-12">
<div class="card">
<div class="card-title">
{{ Auth::user()->name}}
<div class="card-body">
<h1>Thank you for login {{ Auth::user()->name }}</h1>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection