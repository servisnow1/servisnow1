@extends('layouts.app')
@section('title','Narahub - Customer Page')
@section('content')
@include('layouts._include.header')
@foreach($vouchers->chunk(3) as $voucher)
<section id="user-vouchers">
<div class="row mt-2 mb-2">
          @foreach($voucher as $items)
    <div class="col-md-4">
        <a class="card" href="{{ url('myvouchers/details' ,encrypt($items->myVouchers->unique_code)) }}">
        
          <div class="card-body">
            <span class="card-title"><b>{{ $items->name }}</b> </span>
             <p class="justify-content">{{ $items->description }}</p>   
          </div>
          <div class="card-footer">
            <h3 class="text-center text-primary">Rp. {{ number_format($items->price_discount, 2, ',', '.') }}</h3>
            <span>You have : {{ $items->myVouchers->qty }}</span>
            <h3> Expired in :  </h3>
            <p id="clock">{{ $items->expired_on}}</p>
          </div>
        </a>
    </div>
        @endforeach

</div>
    
   @endforeach
@section('scripts')
<script type="text/javascript">
  var finalDate = $('#clock').text();
       $('#clock').countdown(finalDate, function(event) {
          $(this).html(event.strftime('' 
          + '<span>%w</span> Minggu '
    + '<span>%d</span> Hari '
    + '<span>%H</span> Jam <br>'
    + '<span>%M</span> Menit '
    + '<span>%S</span> Detik'
          ));
       });
</script>


@endsection
</section>