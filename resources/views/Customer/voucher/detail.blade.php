@extends('layouts.app')
@section('title', 'User e-voucher')
@section('content')
@include('layouts._include.header')
<section id="user-voucher-details">
<div class="container">
<div class="row mt-2 mb-2">
          <div class="col-md-10">
          <table class="table">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Unique Code</th>
                        
                    </tr>
                </thead>
               
                <tbody>
                    <tr>
                        
                        <td>{{ $isi['name'] or 'Empty' }}</td>
                        <td>Rp {{ number_format($isi['price_discount'], 2, ',', '.') }}</td>
                        
                        
                        @foreach($details as $detail)
                        <td>{{ $detail->qty }}</td>
                        <td>{{ $detail->unique_code }}</td>
                        
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        
                        <td class="small-caps table-bg" style="text-align: right"><b>Bayar di narahub</b></td>
                        <td><b>Rp 0,-</b></td>
                        <td></td>
                    </tr>

                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Your Total</td>
                        <td class="table-bg">Rp {{ number_format($grandTotal, 2, ',', '.') }}</td>
                        
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>
    </div>     
    <div class="col-md-2">
    <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(220)->errorCorrection('H')->generate(url('/redeem/?u=' .encrypt($detail->unique_code) ))) !!} ">
    </div>
</div>
<div class="row">
<div class="col-md-12">
Sold By : {{ $merchants['name'] }}
</div>
</div>
<div class="row">
<div class="col-md-12">
<h2 class="text-center"> Syarat dan Ketentuan</h2>
<hr>
<ol>
<li type="1">Voucher hanya dapat ditukar di cabang merchant yang telah ditentukan.</li>
<li type="1">Voucher berlaku selama belum melewati batas waktu yang telah ditentukan</li>
<li type="1">Pembayaran dilakukan di merchant yang telah ditentukan</li>
<li type="1">Apabila barang mengalami kerusakan silahkan menghubungi pihak merchant secara langsung</li>
<li type="1">Kami tidak bertanggung jawab atas kesalahan yang terjadi dari pihak merchant</li>
</ol>
</div>
</div>
</div>
</section>
@endforeach
@endsection