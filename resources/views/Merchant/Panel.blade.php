<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Merchant - Dashboard</title>

    @include('layouts.style')
  
  </head>

  <body id="page-top">
@include('navbar')
   
<section id="welcome-page">
  <div class="container">
  <div class="row">
    <div class="col-xl-4">
      <div class="card mt-5">
          <div class="card-body">
            <div class="card-title"><h2>Your Viewer Product</h2></div>
            <div class="row ml-2">
            <span><i class="fas fa-eye" style="font-size: 2.7rem;"></i></span>
            &nbsp;&nbsp;
            <h3> {{ number_format(102329, 2 , ',', '.') }} </h3>
            </div>
          </div>
      </div>  
    </div>
    <div class="col-xl-4">
      <div class="card mt-5">
      <div class="card-body">
            <div class="card-title"><h2>Your Transaction</h2></div>
            <div class="row ml-2">
            <span><i class="fas fa-money-bill-alt" style="font-size: 2.7rem;"></i></span>
            &nbsp;&nbsp;
        <h3> {{ number_format(102329, 2 , ',', '.') }} </h3>
            </div>
          </div>
      </div>  
    </div>
    <div class="col-xl-4">
      <div class="card mt-5">
      <div class="card-body">
            <div class="card-title"><h2>Your Transaction</h2></div>
            <div class="row ml-2">
            <span><i class="fas fa-money-bill-alt" style="font-size: 2.7rem;"></i></span>
            &nbsp;&nbsp;
        <h3> {{ number_format(102329, 2 , ',', '.') }} </h3>
            </div>
          </div>
      </div>
      </div>  
    </div>
  </div>

</div>
</section>
@include('layouts._include.plugin')
  </body>

</html>
