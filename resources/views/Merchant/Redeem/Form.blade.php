@extends('layouts.app')
@section('title','Merchant-Redeem Voucher')
@section('content')
@include('navbar')
<div class="row">
    <div class="col-md-12">
        <div class="container">
            <div class="card mt-5 text-center">
            <div class="card-body">
            <form action="{{ route('redeemed') }}" method="POST" id="redeemVoucher">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-4"><label for="Redeem Voucher">Kode Unik Voucher</label></div>
                    <div class="col-md-4">
                    
                    <input type="text" name="kode_unik" class="form-control form-control-lg">
                   
                    </div>
            </div>
            <div class="row mt-2">
                <div class="col-md-12">
                    <button type="submit" value="Redeem" class="btn btn-primary container">Redeem</button>
            
                </div>
                </form>
            </div>
            
            </div>
            
        </div>

        </div>
    </div>
</div>

@section('scripts')
<script type="text/javascript">
$(function(){
$(document).on('submit', 'form#redeemVoucher', function (event) {
    event.preventDefault();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });   
    $.ajax({
        type: form.attr('method'),
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            
                $('#redeemVoucher').find('input:text').val('');
                $.notify(data.message, "success");
               
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
    return false;
});
});
</script>
@endsection