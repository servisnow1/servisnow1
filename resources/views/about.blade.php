<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{app()->getLocale()}}">
    
    <head>
        <title>@yield('title') - Servisnow</title>
        
        @include('layouts._include.head')
        
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" media="screen"/>
        <script src="{{asset('js/jquery-3.1.0-slim.js')}}"></script>
        <script src="{{asset('js/bootstrap.bundle.js')}}"></script>
        
    </head>
    
    <body>
        
        @include('layouts._include.header')
        
        <section>
            <img class="d-block w-100 img-fluid" src="{{asset('img/blog.png')}}" alt="">
        
            <div class="bg-about">
                <h2>what is Servisnow</h2>
                <p>we're a platform that connecting offline stores with online customer by means of online coupon</p>
                <h2>what we do?</h2>
                <p>Local Listing</p>
                <p>Analytic Report</p>
                <h2>how we work</h2>
                <p>small teams</p>
                <ol>
                    <li>Interdisciplanary teams with big ideas</li>
                    <li>Prototyping and local listing from day one</li>
                </ol>
                <p>Client as Partner</p>
                <ol>
                    <li>Clients join</li>
                    <li>Clients join the team to help us connect them with online customer</li>
                </ol>
            </div>
        </section>
    </body>
    
</html>

