@extends('layouts.app')
@section('title','Narahub - Register Form')
@section('content')
@include('layouts._include.header')
<style>
#backgroundRegister{
background-image: url('/img/Register.jpeg'); 
-webkit-background-size: auto;
-moz-background-size: auto;
-o-background-size: auto;
background-size: auto;
overflow-y: hidden;
height: 100vh;
}
</style>
<div class="row">
    <div class="col-md-6" id="backgroundRegister"></div>
<div class="col-md-6">
<div class="container">
    <div class="row">
        <div class="col-md-12 justify-content-center text-center">
            <img src="{{ asset('img/logo.png') }}">
        </div>
    </div>
    <div class="row justify-content-center">
       <h2 class="text-center">{{ __('Register') }}</h2>
            <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        {{ csrf_field() }}
    </div>
                <div class="form-group row mt-4 mb-3">
                    <label for="name" class="col-md-4 col-form-label">{{ __('Name') }}</label>

                            <div class="col-md-8">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-lg" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                </div>

                <div class="form-group row mt-4 mb-3">
                    <label for="email" class="col-md-4 col-form-label">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                </div>

                <div class="form-group row mt-4 mb-3">
                            <label for="phone" class="col-md-4 col-form-label">{{ __('Phone Number') }}</label>

                            <div class="col-md-8">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }} form-control-lg" name="phone" value="{{ old('phone') }}" required>

                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                </div>

                <div class="form-group row mt-4 mb-3">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Password') }}</label>

                            <div class="col-md-8">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                </div>

                

               
                <div class="form-group row">
                           

                    <div class="col-md-12">
                    <button type="submit" class="btn btn-primary container-fluid">
                                    {{ __('Register') }}
                                </button>    
                    </div>
                </div>


                
                
        </div>
</div>
</div>
</div>
@endsection
@section('scripts')
<script>
$(function() 
{
    $('#datetimepicker1').datetimepicker({
        format: 'L'
    });
});

</script>
@endsection
