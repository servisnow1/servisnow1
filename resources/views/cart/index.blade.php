@extends('layouts.app')
@section('title','Cart Content')
@section('content')
@include('layouts._include.header')
@if (sizeof(Cart::getContent()) > 0)

            <table class="table">
                <thead>
                    <tr>
                        <th class="table-image"></th>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th class="column-spacer"></th>
                        <th></th>
                    </tr>
                </thead>
               
                <tbody>
                    @foreach ($details as $item)
                    <tr>
                        <td class="table-image"><a href="{{ url('/vouchers', $item->slug)}}"><img src='{{ asset($item->image)  }}' alt="{{ $item->name }}" width="50px" height="50px" class="img-responsive cart-image"></a></td>
                        <td><a href="{{ url('/vouchers', $item->slug)}}">{{ $item->name }} </a>
                        <td>
                            <select class="quantity" class="form-group form-control" data-id="{{ $item->id }}">
                                <option {{ $item->quantity == 1 ? 'selected' : '' }}>1</option>
                                <option {{ $item->quantity == 2 ? 'selected' : '' }}>2</option>
                                <option {{ $item->quantity == 3 ? 'selected' : '' }}>3</option>
                                <option {{ $item->quantity == 4 ? 'selected' : '' }}>4</option>
                                <option {{ $item->quantity == 5 ? 'selected' : '' }}>5</option>
                            </select>
                        </td>
                        <td>Rp {{ number_format(Cart::get($item->id)->getPriceSum(), 2, ',', '.') }}</td>
                        <td class=""></td>
                        <td>
                        <form action="{{ url('cart', [$item->id]) }}" method="POST" id="formDelete" class="side-by-side">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="submit" class="btn btn-danger btn-sm" value="Remove">
                            </form> 
                            
                        </td>
                    </tr>

                    @endforeach
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right">Subtotal</td>
                        <td>Rp {{ number_format(Cart::getSubTotal(), 2 , ',', '.') }}</td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="table-image"></td>
                        <td></td>
                        <td class="small-caps table-bg" style="text-align: right"><b>Bayar di narahub</b></td>
                        <td><b>Rp 0,-</b></td>
                        <td></td>
                        <td></td>
                    </tr>

                    <tr class="border-bottom">
                        <td class="table-image"></td>
                        <td style="padding: 40px;"></td>
                        <td class="small-caps table-bg" style="text-align: right">Total :</td>
                        <td class="table-bg">Rp {{ number_format(Cart::getTotal(), 2, ',', '.') }}</td>
                        
                        <td class="column-spacer"></td>
                        <td></td>
                    </tr>

                </tbody>
            </table>
            
            <a href="{{ url('/shop/all') }}" class="btn btn-primary btn-lg">Continue Shopping</a> &nbsp;
            
            
            <a href="{{ url('checkout') }}" class="btn btn-success btn-lg">Proceed to Checkout</a>
            
            <div style="float:right" class="mr-2">
                <form action="{{ url('/emptyCart') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="submit" class="btn btn-danger btn-lg" value="Empty Cart">
                </form>
            </div>

        @else

            <h3>You have no items in your shopping cart</h3>
            <a href="{{ url('/shop/all') }}" class="btn btn-primary btn-lg">Continue Shopping</a>

        @endif

        <div class="spacer"></div>

    </div> <!-- end container -->

@endsection

@section('scripts')
    <script>
    $(function(){

            

        $('.quantity').on('change', function() {
                $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                var id = $(this).attr('data-id')
                $.ajax({
                  type: "PATCH",
                  url: '{{ url("/cart") }}' + '/' + id,
                  data: {
                    'quantity': this.value,
                  },
                  success: function(data) {
                    window.location.href = '{{ url("/cart") }}';
                  }
                });

        });

        $('')

        
            
    });

    </script>
@endsection
