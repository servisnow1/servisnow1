<!DOCTYPE html>
<html lang="en">
    
    <head>
        <title>@yield('title') - Servisnow</title>
        
        @include('layouts._include.head')
        
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}" media="screen"/>
        
        <style>
            #map
            {
                height: 400px;
                width: 100%;
            }
        </style>
    </head>
    
    <body>
        @include('layouts._include.header')
        
        <img class="d-block w-100 img-fluid" src="{{asset('img/about.png')}}" alt="">
        
        <section class="bg-contact">
            <div class="row">
                <div class="col-sm">
                    <form>
                        <div class="form-group">
                            <label for="namaLengkap">Nama Lengkap</label>
                            <input type="text" class="form-control" id="namaLengkap" aria-describeby="ininama">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" aria-describeby="ininama">
                        </div>
                        <div class="form-group">
                            <label for="noTelp">Nomer yang bisa dihubungi</label>
                            <input type="tel" class="form-control" id="telepon" aria-describeby="telepon">
                        </div>
                        <button type="submit" class="btn btn-success">Kirim</button>
                    </form>
                </div>
                <div class="col-sm">
                    <div id="map"></div>
                    <script>
                        function initMap()
                        {
                            var kgr = {lat:-7.23999, lng:112.7719799};
                            var map= new.google.maps.Map(document.getElementById('map'), {zoom: 4, center: kgr});
                            var marker = new google.maps.Marker({position: kgr, map: map});
                                       
                        }
                    </script>
                </div>
            </div>
        </section>
        
        <footer>
            <h3 class="display-6">copyright@servisnow.com 2018</h3>
        </footer>
    </body>
    
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHh2PuuBgqB7dG0K3vo_3KtATpvvBcR8E&callback=initMap">
    </script>
</html>