@extends('layouts.app')

@section('content')
@include('layouts._include.header')
<div class="container-fluid  mt-4">
<!-- Start Section carousel images -->

<div class="row">
  <div class="col-md-8">
    <section id="carousel-image-vouchers">
      <div id="carouselControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          @foreach($images as $image)
            <div class="carousel-item {{ $loop->first ? 'active' : ''}} ">
              <img class="d-block w-100" src="{{ asset('images/' .$vouchers['merchant_id']. '/' .$image->name ) }}" width="600px" height="500px" alt="Narahub Images">
            </div>
          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color:#000;"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true" style="background-color:#000;"></span>
          <span class="sr-only">Next</span>
        </a>
        </div>
      </section>
    </div>
<div class="col-md-4">
  
      <div class="card">
        <h2 class="text-center p-3">{{ $vouchers['name'] }} <span class="badge {{ $vouchers['value'] > 0 ? 'badge-success' : 'badge-danger' }}" style="font-size: 0.8rem;">{{ $vouchers['value'] > 0 ? 'In stock' : 'Out of stock' }}</span></h2>
                <span class="pl-3"><del class="text-danger">Rp. {{ number_format($vouchers['price_normal'], 2, ',', '.') }}<del></span>
                <h2 class="pl-3">Rp. {{ number_format($vouchers['price_discount'], 2, ',', '.') }}</h3>
                <br>
                <p class="pl-3" style="font-size: 1.2rem; line-height: 0;"><span class="badge badge-success badge-lg"><i class="far fa-clock"></i>&nbsp;&nbsp;Penawaran Terbatas</span></p>
                <p class="pl-3" style="font-size: 1.2rem; line-height: 0;"><span class="badge badge-success badge-lg"><i class="far fa-money-bill-alt"></i>&nbsp;&nbsp;Harga Terbaik</span></p>
                <p class="pl-3">Qty:</p>
                <form action="{{ url('cart') }}" id="formDetails" method="POST">
                {{ csrf_field() }}
                <input type="hidden" value="{{ encrypt($vouchers['id'] ) }}" name="id">
                <input type="hidden" value="{{ encrypt($vouchers['name']) }}" name="name">
                <input type="hidden" value="{{ encrypt(asset('images/' .$vouchers['merchant_id']. '/' .$image->name )) }}" name="image">
                <input type="hidden" value="{{ encrypt($vouchers['slug']) }}" name="slug">
                <input type="hidden" value="{{ encrypt($vouchers['price_normal']) }}" name="priceNormal">
                <input type="hidden" value="{{ encrypt($vouchers['price_discount']) }}" name="priceDiscount">
              <select name="Qty" class="form-group form-control-lg ml-3" id="">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
              </select>
              <br>
             
        <div class="container">
            <button type="submit"  class="btn btn-primary form-group form-control mt-2">Add to bag</button>
        </div>
        </form>
    </div>

<div class="row mt-2 ml-1">
<div class="card">
<div class="card-body"><h3>Waktu penawaran tinggal :</h3>

<p id="clock" style="font-size:1.6rem" class="text-center" class="text-danger">{{ $vouchers['expired_on'] }}</p>

</div>

</div>
</div>
</div>
</div>
<!-- End Section carousel images -->

<!-- Start Section description detail -->
<div class="row mt-4">
<div class="col-md-12">
<section id="details-description">
<div class="card">

<div class="card-body">
  <h1 class="text-center">Informasi</h1>
{!! $vouchers['details'] !!}
</div>
</div>
</section>
</div>
</div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(function () {
$(document).on('submit', 'form#formDetails', function (event) {
    event.preventDefault();
    var form = $(this);
    var data = new FormData($(this)[0]);
    var url = form.attr("action");
    $.ajax({
        type: form.attr('method'),
        url: url,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {

                $('#cart-value').text(data.jumlah);
                $.notify("Berhasil ditambahkan ke goody bag", "success");
               
            },
        error: function (xhr, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
    return false;
});

  
    
       var finalDate = $('#clock').text();
       $('#clock').countdown(finalDate, function(event) {
          $(this).html(event.strftime('' 
          + '<span>%w</span> Minggu '
    + '<span>%d</span> Hari '
    + '<span>%H</span> Jam <br>'
    + '<span>%M</span> Menit '
    + '<span>%S</span> Detik'
          ));
       });
     

});
</script>
@endsection