@extends('layouts.app')
@section('title','One stop IT Offer Deals - NaraHub')
@section('content')
@include('layouts._include.header')
<section id="carousel-homepage">
@include('layouts._include.carousel')
</section>
<!-- Section Voucher -->
<section class="container pt-3 mb-3" id="voucher">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="text-center">Offer Deals</h2>
            <div class="col-sm-12 text-md-right lead">
                <a class="btn btn-primary" href="{{ url('shop/all') }}" title="Lihat Semua">Lihat Semua</a>
            </div>
        </div>
    </div>
    @foreach($vouchers->chunk(3) as $voucher)
    <div class="row mt-5 mb-2">
    @foreach($voucher as $items)
       <div class="col-md-4">
        
    
          
        <a href="{{ url('/vouchers/'.$items->slug)}}" class="card">
             <img class="card-img"  src="{{ asset('images/' .$items->merchant_id. '/'.$items->images[0]->name ) }}" alt="Merchant1">
          <div class="card-body">
            <h5 class="card-title">{{ $items->name }} </h5>
                <p>{{ $items->description}}</p>
          </div>
          <div class="card-footer" style="background-color:white;">
            <p class="text-right mb-0"><del> Rp. {{ number_format($items->price_normal, 2, ',', '.') }}<del></p>
            <h3 class="text-right text-primary">Rp. {{ number_format($items->price_discount, 2, ',', '.') }}</h3>
          </div>
        </a>
      </div>
    @endforeach

      </div>
    </div>
   @endforeach
</section>
<!-- End Section -->




<!-- End Benefit Section -->
<!-- Join Section -->
<section id="Join">


<div class="card">
<div class="card-body">
<div class="container-fluid my-5">
  <h2 class="text-center card-title">Gabung dengan kami</h2>
<p class="card-text">Ingin bisnis anda berkembang? gabung dengan kami sekarang. cukup masukkan alamat email anda, maka kami akan segera menghubungi anda. </p>
  <div class="form-group">
    <input type="text" class="form-control" placeholder="Enter your email address...">
  </div><!-- /form-group -->

  <div class-fluid="row">
    <div class="col-sm-12">
      <button class="btn btn-primary btn-block">Saya Mau</button>
    </div><!-- /col -->
  </div><!-- /row -->
  </div>
  </div>
</div>
</section>
<!-- End Join Section -->
<!-- Footer Section -->
<footer class="footer">
<div class="container bottom_border">
<div class="row">
<div class=" col-sm-4 col-md col-sm-4  col-12 col">
<h5 class="headin5_amrc col_white_amrc pt2">Find us</h5>
<!--headin5_amrc-->
<address>
<p class="mb10">Surabaya City, East java, Indonesia</p>
<p><i class="fa fa-location-arrow"></i>7° 16' 33.5028'' S</p>
<p><i class="fa fa-phone"></i>  +62-82301304452  </p>
<p><i class="fa fa fa-envelope"></i>info.servisnow@gmail.com</p>
</address>

</div>



<div class=" col-sm-4 col-md  col-6 col">
<h5 class="headin5_amrc col_white_amrc pt2">Site links</h5>
<!--headin5_amrc-->
<ul class="footer_ul_amrc">
<li><a href="#">Home</a></li>
<li><a href="#">Categories</a></li>
<li><a href="#">Site Map</a></li>
<li><a href="#">Career</a></li>
<li><a href="#">Our Teams</a></li>
<li><a href="#">Testimonials</a></li>
<li><a href="#">Merchants</a></li>
<li><a href="#">Sponsorship</a></li>
</ul>
<!--footer_ul_amrc ends here-->
</div>


<div class=" col-sm-4 col-md  col-12 col">
<h5 class="headin5_amrc col_white_amrc pt2">Follow us</h5>
<!--headin5_amrc ends here-->

<ul class="footer_ul2_amrc">
<li><a href="#"><i class="fab fa-facebook fleft padding-right"></i> </a><p><a href="https://www.facebook.com/iniservisnow">Facebook</a></p></li>
<li><a href="#"><i class="fab fa-twitter fleft padding-right"></i> </a><p><a href="#">twitter</a></p></li>
<li><a href="#"><i class="fab fa-instagram fleft padding-right"></i> </a><p><a href="#">instagram</a></p></li>
</ul>
<!--footer_ul2_amrc ends here-->
</div>
</div>
</div>


<div class="container">

<!--foote_bottom_ul_amrc ends here-->
<p class="text-center">Copyright &copy; 2018 | Design with <span color="red">&#x2764;</span> by <a href=" {{ url('/') }}">Narahub</a></p>

<!--social_footer_ul ends here-->
</div>

</footer>
<!-- End Footer -->
@endsection
@section('scripts')
<script type="text/javascript">
(function($) {
    "use strict";

    // manual carousel controls
    $('.carousel-control-next').click(function(){ $('.carousel').carousel('next');return false; });
    $('.carousel-control-prev').click(function(){ $('.carousel').carousel('prev');return false; });
    
})(jQuery);
$(function(){
  $(".col-md-4").hover(
    function(){
    $(this).animate({
      marginTop: "-=1%",
    },200);
  },
  function(){
    $(this).animate({
      marginTop: "0%",
    },200);
  }
);
});

</script>




@endsection