<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
</head>
<style>
     .navbar-brand{
        font-family: 'Pacifico', cursive;
        font-size : 24px;
      }

      .nav-link{
        font-size : 16px;
      }
    </style>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-narahub-accent sticky-top">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
  <ul class="navbar-nav">
<a class="navbar-brand" href="#"><img src="{{ asset('img/logo.png') }}" alt="Servis Now" style="width: 50px; height: 50px; font-family: 'Pacifico', cursive;">&nbsp <label style="color:white;">Narahub</label></a>

  </ul>  
  <ul class="navbar-nav ml-auto">
  <form class="form-inline">
    <input class="form-control" type="search" placeholder="Search" aria-label="Search">
    &nbsp <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
  </form>

  </ul>
  @auth
  <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/') }}" style="color:white;">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/users/profile') }}" style="color:white;">Profile</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('myvouchers' ,Auth::user()->id) }}" style="color:white;">My Vouchers</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('shop/all') }}" style="color:white;">Vouchers</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" style="color:white;">Article</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('cart.index') }}" style="color:white;"><i class="fas fa-shopping-bag"></i>Cart<span class="badge badge-pill badge-danger" id="cart-value">{{ Cart::getContent()->count() }}</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/users/setting') }}" style="color:white;">Settings</a>
      </li>
      <li class="nav-item">
      <form id="logout-form" action="{{ route('logout') }}" method="POST">
      {{ csrf_field() }}
        <button type="submit" value="Logout" class="btn btn-danger btn-lg">Logout</button>
        </form>
      </li>
     
     
    
      
</ul>
@endauth
  @guest
  <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('/') }}" style="color:white;">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" style="color:white;">Lelang</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ url('shop/all') }}" style="color:white;">Vouchers</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" style="color:white;">Article</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('cart.index') }}" style="color:white;"><i class="fas fa-shopping-bag"></i>My Bag<span class="badge badge-pill badge-danger" id="cart-value">{{ Cart::getContent()->count() }}</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link btn btn-info" href="{{ url('/register') }}" style="color:white; border-radius: 10px;">Sign Up</a>
      </li>
      &nbsp;
      
      <li class="nav-item">
        <a class="nav-link btn btn-success" href="{{ url('/landingpage') }}" style="color:white; border-radius: 10px;">Login</a>
      </li>
</ul>
@endguest
  </div>
</nav>
</body>
</html>
