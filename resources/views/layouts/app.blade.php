<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie9" xmlns="http://www.w3.org/1999/xhtml" lang="en-US"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ app()->getLocale() }}">
<!--<![endif]-->
<head>
<title>@yield('title', config('app.name'))</title>
	
    @include('layouts._include.head')

	<!-- Styles -->
    @include('layouts.style')
    <style>
    body {
        font-family : 'open sans', sans-serif;
    }
    </style>
	<!-- end Styles -->
    <link rel="icon" href="{{ asset('img/logo.png') }}"/>
</head>
<body>
<main id="content">

        <!-- Layout -->
	
        
		@yield('content')
        
		

	<!-- end layout -->
    </main>
    <!-- Scripts -->
    @include('layouts._include.plugin')
    

    @include('layouts._include.g_analytics')
@yield('scripts')
</body>
</html>