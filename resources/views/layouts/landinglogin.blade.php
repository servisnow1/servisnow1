<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Narahub - Landing page</title>

    @include('layouts.style')
    <style>
    body {
  height: 100vh;
}
    </style>
</head>
<body id="landing-page">
<main>
<section>
<div class="container h-100 mt-5">
    <div class="row justify-content-center text-center align-items-center">
        <div class="col-sm-12">
            <br><br><br><br><br><br><br><br>
            <h1 class="text-white" style=" font-family: 'Pacifico', cursive;
        font-size : 60px;">Narahub</h1>
        <h3 class="text-white">Welcome to narahub. Please login as</h3>
        </div>
    </div>
    <div class="row justify-content-center align-items-center">
    <a href="{{ route('login') }}" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i>&nbspLogin as customer</a>
    &nbsp
    <a href="{{ route('merchant.loginform') }}" class="btn btn-success"><i class="fas fa-sign-in-alt"></i>&nbspLogin as merchant</a>
    </div>
    <div class="row mt-3 justify-content-center align-items-center">
    <a href="{{ url('/') }}" class="btn btn-warning text-white">Back</a>
    </div>
</div>
</section>
<main>
</body>
</html>