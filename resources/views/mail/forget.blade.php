@extends('layouts.app')
@section('title','Merchants Forget Password - NaraHub')
@section('content')
@include('mail.header')
Halo, kami menerima permintaan untuk melakukan reset password atas email : {{ $email }} . Klik tombol berikut untuk melakukan proses reset password selanjutnya : 
<br><br><br>

<a href="{{ url('/reset/passwords/merchants/' .$token) }} " class="btn btn-primary">Ubah Password</a>

<b>NB : Abaikan jika anda tidak merasa melakukan permintaan untuk mengganti password.</b>
<br><br>

Regards, <br><br> Narahub

@endsection
