@extends('layouts.app')
@section('content')
<nav class="navbar navbar-light bg-primary">
  <a class="navbar-brand">
    <img src="{{ asset('img/logo.png') }}" width="30" height="30" class="d-inline-block align-top" alt="">
    Narahub
  </a>
</nav>