<div class="container-fluid" style="padding: 0;">
<div class="row">
<div class="col-md-12">
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
  @auth("admin")
  <a class="navbar-brand text-white" href="{{ url('admin/') }}">Admin - Narahub</a>
  @endauth
  @auth("merchant")
  <a class="navbar-brand text-white" href="{{ url('merchant/') }}">{{ Auth::user()->name }} - Narahub</a>
  @endauth
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  @auth("admin")  
  <ul class="navbar-nav ml-auto">
     
      <li class="nav-item active">
        <a class="nav-link text-white" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="{{ route('admin.merchants') }}">
          Merchants
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="{{ route('admin.categories') }}">
          Categories
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="{{ route('admin.vouchers') }}">
          Vouchers
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white">
          Auctions
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Reports
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-bell"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
         <span class="dropdown-item">You don't have notification</span>
          
          
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-user-circle"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Settings</a>
          <a class="dropdown-item" href="{{ route('admin.logout') }}">Logout</a>
        </div>
      </li>
      
     

    </ul>
    @endauth
    @auth("merchant")
    <ul class="navbar-nav ml-auto">
     
      <li class="nav-item active">
        <a class="nav-link text-white" href="{{ route('merchant.dashboard') }}">Home </a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="{{ route('admin.merchants') }}">
          Voucher
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="{{ route('admin.merchants') }}">
          Lelang
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="{{ route('redeem') }}">
          Redeem Voucher
        </a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Reports
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item text-white" href="{{ url('redeem/') }}">Voucher Redeem</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-bell"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <span class="dropdown-item text-white" href="#">Action</a>
          <span class="dropdown-item text-white" href="#">Another action</a>
          
          
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="far fa-user-circle"></i>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Settings</a>
          <a class="dropdown-item" href="{{ url('merchant/logout') }}">Logout</a>
        </div>
      </li>
      
     

    </ul>
    @endauth
  </div>
</nav>
</div>
</div>
</div>
