@extends('layouts.app')

@section('content')
@include('layouts._include.header')




<!-- Section Voucher -->
<section class="container pt-3" id="voucher">
    <div class="row">
        <div class="col-sm-12">
            <h2 class="text-center">All Vouchers</h2>
            
        </div>
    </div>
    <div class="loading text-center" style="display: none">
    </div>
@foreach($vouchers->chunk(3) as $voucher)
<div class="row mt-5 mb-2">

    @foreach($voucher as $items)
    <div class="col-md-4">
    <a href="{{ url('/vouchers/'.$items->slug)}}" class="card">
             <img class="card-img"  src="{{ asset('images/' .$items->merchant_id. '/'.$items->images[0]->name ) }}" alt="Merchant1">
          <div class="card-body">
            <h5 class="card-title">{{ $items->name }} </h5>
                <p>{{ $items->description}}</p>
          </div>
          <div class="card-footer" style="background-color:white;">
            <p class="text-right mb-0"><del> Rp. {{ number_format($items->price_normal, 2, ',', '.') }}<del></p>
            <h3 class="text-right text-primary">Rp. {{ number_format($items->price_discount, 2, ',', '.') }}</h3>
          </div>
        </a>
        
    </div>
        @endforeach
            


</div>

@endforeach   
{{ $vouchers->links() }}
</section>

<!-- End Section -->
@endsection
@section('scripts')
<script type="text/javascript">
   $(function(){
       $('.pagination').css('display: none; !important');
       $(document).on('click', 'a.page-link', function (event) {
    event.preventDefault();
    var url = $(this).attr('href'); 
    window.history.pushState("", "", url);
    
                
    ajaxLoad(url);
});

function ajaxLoad(filename, content) {
    content = typeof content !== 'undefined' ? content : 'content';
    
        $('.loading').append('<img style="position : absolute;  z-index: 100000;" src="/img/Loading.gif" />');
        $('.loading').show();
    
    
    
    $.ajax({
        type: "GET",
        url: filename,
        contentType: false,
        success: function (data) {
            $("#" + content).html(data);
            $('.loading').hide();
        },
        error: function (xhr, status, error) {
            alert(xhr.responseText);
        }
    });
}
$(".col-md-4").hover(
    function(){
    $(this).animate({
      marginTop: "-=1%",
    },200);
  },
  function(){
    $(this).animate({
      marginTop: "0%",
    },200);
  }
);
   });
</script>
@endsection
