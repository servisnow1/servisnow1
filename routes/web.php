<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Home\IndexController@index');

Auth::routes();
Route::post('register/users','Register\RegisterUsersController@createUsers')->name('users.register');
Route::get('/home', 'HomeController@index')->name('home');
//Route::get('auth/redirect', 'Auth\LoginController@redirectToProvider');
//Route::get('auth/callback', 'Auth\LoginController@handleProviderCallback');

/* Admin Route */
Route::prefix('admin')->group(function() {
    Route::get('/login',
    'Admin\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Admin\AdminLoginController@login')->name('admin.login.submit');
    Route::get('logout/', 'Admin\AdminLoginController@logout')->name('admin.logout');
     
    Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
     /* For admin merchant routes */
     Route::get('/merchants','Admin\AdminMerchantController@showMerchants')->name('admin.merchants');
     Route::get('/getindexmerchants','Admin\AdminMerchantController@indexMerchants')->name('merchants.index');
     Route::get('/merchants/create','Admin\AdminMerchantController@showForm')->name('create.merchant');
     Route::post('/merchants/create','Admin\AdminMerchantController@storeMerchant')->name('store.merchant');   

     /* For admin categories routes */
     Route::get('/getindexcategories','Admin\AdminCategoriesController@indexCategories')->name('index.categories');
     Route::get('/categories/create','Admin\AdminCategoriesController@showFormCategory')->name('create.categories');
     Route::post('/categories/create','Admin\AdminCategoriesController@storeCategory')->name('store.categories');
     Route::get('/categories','Admin\AdminCategoriesController@showCategories')->name('admin.categories');
     /* End admin categories routes */

     /* For admin vouchers routes */
     Route::get('/vouchers','Admin\AdminVoucherController@indexVouchers')->name('admin.vouchers');
     Route::get('/vouchers/create','Admin\AdminVoucherController@showFormVouchers')->name('create.vouchers');
     Route::post('/vouchers/store','Admin\AdminVoucherController@storeVouchers')->name('store.vouchers');
     /* End Admin Vouchers controller */   
    }); 
/* End Admin Route */

/* Merchants Back End Route */
Route::prefix('merchant')->group(function() {
    Route::get('/login',
    'Merchant\MerchantLoginController@showLoginForm')->name('merchant.loginform');
    Route::get('/', 'Merchant\MerchantController@index')->name('merchant.dashboard');
    Route::post('/login', 'Merchant\MerchantLoginController@login')->name('merchant.submit');
    Route::get('logout/', 'Merchant\MerchantLoginController@logout')->name('merchant.logout');
    Route::get('/reset','Merchant\ResetPasswordController@resetView')->name('merchant.resetpassword');
    Route::post('/reset','Merchant\ResetPasswordController@saveData')->name('forget.password.merchant');
}); 

/* End Merchants Back End Route */

Route::get('/landingpage', function(){
    return view('layouts.landinglogin');
    });
Route::get('/vouchers/{slug}','Home\IndexController@showDetails');
Route::get('shop/all', 'Home\IndexController@shopAll');
Route::delete('emptyCart', 'Cart\CartController@emptyCart');
Route::get('checkout','Cart\CartController@checkOut');
Route::resource('cart','Cart\CartController');
Route::get('myvouchers/{id}',['uses' => 'HomeController@myVouchers', 'middleware' => 'AuthResource','name' => 'CustomerVouchers']);
Route::get('myvouchers/details/{uniqueCode}','HomeController@myVoucherDetails');
Route::post('/redeem/?u={kode_unik}','Merchant\RedeemController@redeemVoucher');
Route::get('/redeem','Merchant\RedeemController@redeemView')->name('redeem');
Route::post('/redeem','Merchant\RedeemController@redeemVoucherForm')->name('redeemed');

Route::prefix('users')->group(function() {
    Route::get('/profile/{id}',
    'Merchant\MerchantLoginController@showLoginForm')->name('merchant.loginform');
}); 
Route::Auth();
